# 使用kubeflow训练并预测mnist数据集

最近在研究如何使用kubeflow来进行深度学习的训练，所以先试了一下mnist数据集的训练，用到的框架为pytorch，由于没有GPU环境，因此使用CPU版的，迭代次数小的话，也不会很慢。


包括三个组件，load_data（加载数据），train（训练），predict（预测）



其中挂载数据卷用到了网络文件服务器NFS（可以实现不同服务器之间的目录挂载）,在容器和宿主机间共享一个目录。

每个组件的定义包括输入输出，使用的镜像，在容器中要执行的命令，加载数据将数据分为训练和预测的，训练的传给train组件，预测的传给predict，train阶段得到的模型也传给predict。


每个组件的dockerfile同级目录包括组件的代码，dockerfile文件，以及组件所依赖的包。


执行以下命令构建镜像，三个组件同理。

docker build -t mnist-load_data:v0.0.1 .

制作好三个组件的镜像，然后编译pipeline代码生成yaml文件，将yaml上传到kubeflow UI界面，然后创建运行，填写好参数，点击运行。

最终预测输出为一个csv文件，左边是图片编号，右边是预测值。