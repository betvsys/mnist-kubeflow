from __future__ import absolute_import, division, print_function, \
    unicode_literals
import sys
sys.path.append("..")
import argparse
import numpy as np
import torch
from torch import nn, optim
from torch.autograd import Variable
from torch.utils.data.dataset import Dataset
from torchvision import datasets, transforms
import pandas as pd
from uuid import uuid1
from nfs.aiflow.report import *

from torch.utils.data import DataLoader

class DealDataset(Dataset):
    """
        读取数据、初始化数据
    """
    def __init__(self,data_name, label_name,transform=None):
        (train_set, train_labels) = load_data(data_name, label_name) # 其实也可以直接使用torch.load(),读取之后的结果为torch.Tensor形式
        self.train_set = train_set
        self.train_labels = train_labels
        self.transform = transform

    def __getitem__(self, index):

        img, target = self.train_set[index], int(self.train_labels[index])
        if self.transform is not None:
            img = self.transform(img)
        return img, target

    def __len__(self):
        return len(self.train_set)


def load_data(data_name, label_name):
    """
        data_folder: 文件目录
        data_name： 数据文件名
        label_name：标签数据文件名
    """
    # lbpath = os.path.join(data_folder,label_name)
    y_train = np.load(label_name)

    # imgpath = os.path.join(data_folder, data_name)
    x_train = np.load(data_name)

    return (x_train, y_train)

class CNN(nn.Module):

    def __init__(self):

        super(CNN, self).__init__()

        self.layer1 = nn.Sequential(

            nn.Conv2d(1, 25, kernel_size=3),

            nn.BatchNorm2d(25),

            nn.ReLU(inplace=True)

        )

        self.layer2 = nn.Sequential(

            nn.MaxPool2d(kernel_size=2, stride=2)

        )

        self.layer3 = nn.Sequential(

            nn.Conv2d(25, 50, kernel_size=3),

            nn.BatchNorm2d(50),

            nn.ReLU(inplace=True)

        )

        self.layer4 = nn.Sequential(

            nn.MaxPool2d(kernel_size=2, stride=2)

        )

        self.fc = nn.Sequential(

            nn.Linear(50 * 5 * 5, 1024),

            nn.ReLU(inplace=True),

            nn.Linear(1024, 128),

            nn.ReLU(inplace=True),

            nn.Linear(128, 10)

        )

    def forward(self, x):

        x = self.layer1(x)

        x = self.layer2(x)

        x = self.layer3(x)

        x = self.layer4(x)

        x = x.view(x.size(0), -1)

        x = self.fc(x)

        return x

# train model
def train_model(model_path, data_file, output_dir):
    """
    all file use absolute dir
    :param data_file: `train_test_data.txt` absolute dir
    data_file: 里面存的是mnist数据集的路径，eg:MNIST_data/
    :param output_dir:
    :return:
    """

    # 选择模型
    model = CNN()
    stat_dict = torch.load(model_path)
    model.load_state_dict(stat_dict)

    # if torch.cuda.is_available():
    #     print("CUDA is available")
    #     model = model.cuda()

    # 定义损失函数和优化器

    # with open(data_file, 'r') as f:
    #     data_list = f.readlines()[0].split(',')
    # 如果kubeflow自动读取txt文件内容，则注释上面两行，解开下面这行的注释
    data_list = data_file.split(',')

    test_dataset = DealDataset(data_list[1], data_list[3],
                               transform=transforms.ToTensor())

    test_loader = DataLoader(
        dataset=test_dataset,
        batch_size=1,
        shuffle=False,
    )


    # 模型測試
    result = []
    model.eval()

    eval_loss = 0

    eval_acc = 0

    i = 0

    for data in test_loader:

        img, label = data
        # print(label)

        img = img.float()

        img = Variable(img)

        # if torch.cuda.is_available():
        #     img = img.cuda()
        #
        #     label = label.cuda()

        out = model(img)

        _, pred = torch.max(out, 1)

        result.append(pred[0].cpu().numpy())

        i+=1

        num_correct = (pred == label).sum()

        eval_acc += num_correct.item()

    output_name = str(uuid1())  # 唯一文件名
    pd.DataFrame(columns=['num'],data=result).to_csv(output_dir+"/"+output_name+".csv")
    print('Test Loss: {:.6f}, Acc: {:.6f}'.format(

        eval_loss / (len(test_dataset)),

        eval_acc / (len(test_dataset))
    ))


    with open(output_dir + "/" + 'predict.txt', 'w') as f:
        f.write(output_dir+"/"+output_name+".csv")
    print("### write result csv file to:"+output_dir+"/"+output_name+".csv")
    # 汇报结果存放地址
    # report_data(100, 100, "asfasf", output_dir+"/"+output_name+".csv")


def parse_arguments():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(description='Kubeflow MNIST train model script')
    parser.add_argument('--data_dir', type=str, required=True, help='output dir')
    parser.add_argument('--data_file', type=str, required=True, help='a file write dataset file dir')
    parser.add_argument('--model_path', type=str, required=True, help='model path')
    args = parser.parse_args()
    return args


def run():
    args = parse_arguments()
    train_model(args.model_path,args.data_file, args.data_dir)


if __name__ == '__main__':
    run()
